import { apiConstants } from '../constants';
import { serverService } from '../services';


export const serverAction = {
    getAll
};

function getAll(ctx) {
    return serverService.getAll(ctx)
        .then(
            res => {
                return { type: apiConstants.GETALL_SUCCESS, res };
            },
            error => {
                return { type: apiConstants.GETALL_FAILURE, error }
            }
        );
}


import * as _ from 'underscore';
import moment from 'moment';

import { apiConstants } from '../constants';
import { statService } from '../services';
import { result } from 'underscore';



export const statAction = {
    getPlayerBalance,
    getStatRakeReports,
    getStatFromTo,
    getWalletBalance
};



function getPlayerBalance(ctx) {
    return statService.getPlayerBalance(ctx)
        .then(
            res => {
                return { type: apiConstants.GETALL_SUCCESS, res };
            },
            error => {
                return { type: apiConstants.GETALL_FAILURE, error }
            }
        );
}

async function getStatRakeReports(ctx) {
    return statService.getStatReports(ctx).then(data => {

        const types = { affiliatePayouts: 0, salaries: 0, playersCredits: 0, rakeback: 0, grossRake: 0, tournamentRegular: 0, tournamentSngs: 0, tournamentFreeRolls: 0, netRake: 0 };

        let results = {
            latestWeek: types,
            lastWeek: types,
            lastTwoWeeks: types,
            latestMonth: types,
            lastMonth: types,
            lastTwoMonths: types,
        };

        if (_.size(data)) {
            results = {
                ...results,
                latestWeek: formatStatResult(data, 'rolling-week'),
                lastWeek: formatStatResult(data, 'rolling-last-week'),
                latestMonth: formatStatResult(data, 'rolling-month'),
                lastTwoWeeks: formatStatResult(data, 'rolling-2-weeks-ago'),
                lastMonth: formatStatResult(data, 'rolling-last-month'),
                lastTwoMonths: formatStatResult(data, 'rolling-2-months-ago'),
            };
        }

        return handleRespone(results);
    });
}


async function getStatFromTo(from, to) {

    let start = moment(from, "YYYY-MM-DD");
    let end = moment(to, "YYYY-MM-DD");

    return statService.getStatFromTo(start, end).then(data => {

        const result = [];

        if (!_.isEmpty(data['custom-period'])) {
            const res = data['custom-period'];
            const affiliatePayout = joinStatResult(res, 'affiliate-payouts');
            const grossRake = joinStatResult(res, 'gross-rake');
            const salaries = joinStatResult(res, 'salaries');
            const playerCredits = joinStatResult(res, 'player-credits');
            const rakeback = joinStatResult(res, 'rakeback');
            const tournamentRegular = joinStatResult(res, 'tournament-regular');
            const tournamentSngs = joinStatResult(res, 'tournament-sngs');
            const tournamentFreeRolls = joinStatResult(res, 'tournamentFreeRolls');

            const dates = _.unique(_.pluck(_.union(affiliatePayout, grossRake, salaries, playerCredits, rakeback, tournamentRegular, tournamentSngs, tournamentFreeRolls), 'createdAt'));

            _.each(dates, (date) => {

                const affiliateAmount = _.find(affiliatePayout, function (item) { return item.createdAt == date; });
                const grossRakeAmount = _.find(grossRake, function (item) { return item.createdAt == date; });
                const salariesAmount = _.find(salaries, function (item) { return item.createdAt == date; });
                const playerCreditsAmount = _.find(playerCredits, function (item) { return item.createdAt == date; });
                const rakebackAmount = _.find(rakeback, function (item) { return item.createdAt == date; });
                const tournamentRegularAmount = _.find(tournamentRegular, function (item) { return item.createdAt == date; });
                const tournamentSngsAmount = _.find(tournamentSngs, function (item) { return item.createdAt == date; });
                const tournamentFreeRollsAmount = _.find(tournamentFreeRolls, function (item) { return item.createdAt == date; });

                result[date] = {
                    affiliatePayout: affiliateAmount ? affiliateAmount.amount : 0,
                    grossRake: grossRakeAmount ? grossRakeAmount.amount : 0,
                    salaries: salariesAmount ? salariesAmount.amount : 0,
                    playerCredits: playerCreditsAmount ? playerCreditsAmount.amount : 0,
                    rakeback: rakebackAmount ? rakebackAmount.amount : 0,
                    tournamentRegular: tournamentRegularAmount ? tournamentRegularAmount.amount : 0,
                    tournamentSngs: tournamentSngsAmount ? tournamentSngsAmount.amount : 0,
                    tournamentFreeRolls: tournamentFreeRollsAmount ? tournamentFreeRollsAmount.amount : 0
                }
            });
        }
        return handleRespone(result);
    }).catch(error => handleError(error));
}

async function getWalletBalance(ctx) {
    return statService.getBalanceWallet(ctx).then(data => handleRespone(data)).catch(error => handleError(error));
}

function joinStatResult(data, key) {
    if (!data[key]) {
        return { createdAt: false, amount: 0 };
    }
    return Object.values(data[key].reduce((r, o) => {

        const createdAt = moment(o.createdAt).utc().format("YYYY-MM-DD");

        r[createdAt] = r[createdAt] || { createdAt: createdAt, amount: 0 };;
        r[createdAt].amount += +o.amount;

        return r;
    }, {}));
}

function fromatStatAmount(data, key) {
    return data.hasOwnProperty(key) ? _.reduce(data[key], function (memo, num) { return num.amount + memo; }, 0) : 0;
}

function formatStatResult(data, period) {
    return {
        affiliatePayouts: fromatStatAmount(data[period], 'affiliate-payouts'),
        salaries: fromatStatAmount(data[period], 'salaries'),
        playersCredits: fromatStatAmount(data[period], 'player-credits'),
        rakeback: fromatStatAmount(data[period], 'rakeback'),
        grossRake: fromatStatAmount(data[period], 'gross-rake'),
        tournamentRegular: fromatStatAmount(data[period], 'tournament-regular'),
        tournamentSngs: fromatStatAmount(data[period], 'tournament-sngs'),
        tournamentFreeRolls: fromatStatAmount(data[period], 'tournament- freerolls'),
        netRake: fromatStatAmount(data[period], 'affiliate-payouts') +
            fromatStatAmount(data[period], 'salaries') +
            fromatStatAmount(data[period], 'player-credits') +
            fromatStatAmount(data[period], 'rakeback') +
            fromatStatAmount(data[period], 'gross-rake') +
            fromatStatAmount(data[period], 'tournament-regular') +
            fromatStatAmount(data[period], 'tournament-sngs') +
            fromatStatAmount(data[period], 'tournament- freerolls')
    }
}


function handleRespone(res) {
    return { type: apiConstants.GETALL_SUCCESS, res };
}

function handleError(error) {
    return { type: apiConstants.GETALL_FAILURE, error }
}


import { userConstants } from '../constants';
import { userService } from '../services';


export const userActions = {
    login
};

function login(username, password) {
    return userService.login(username, password)
        .then(
            user => {

                userService.setToken(user.token);
                return { type: userConstants.LOGIN_SUCCESS, user }
            },
            error => {
                return { type: userConstants.LOGIN_FAILURE, error }
            }
        );
}


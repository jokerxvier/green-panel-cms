import Document, { Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
    render() {
        return (
            <html>
                <title>SWC - Green Panel</title>
                <Head>
                    <link href="/static/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
                    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
                </Head>
                <body id="page-top" className="bg-gradient-success">
                    <Main />
                    <NextScript />

                    <script src="/static/jquery/jquery.min.js"></script>
                    <script src="/static/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <script src="/static/jquery-easing/jquery.easing.min.js"></script>
                    <script src="/static/sb-admin-2.min.js"></script>
                </body>
            </html>
        )
    }
}
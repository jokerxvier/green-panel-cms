import Router from 'next/router';
import { Formik, useFormik } from 'formik';
import * as Yup from "yup";
import GuessLayoutHoc from '../components/Layouts/GuessLayoutHoc';
import { userConstants } from '../constants';
import { userActions } from '../actions';


const validationSchema = Yup.object({
    username: Yup.string().required('Required'),
    password: Yup.string().required("Required")
});

export default function Home() {

    const { handleSubmit, handleChange, values, errors, status } = useFormik({
        initialValues: {
            username: "",
            password: ""
        },
        validationSchema,
        onSubmit(values, actions) {

            if (values.username && values.password) {
                userActions.login(values.username, values.password).then(data => {
                    if (data.type !== userConstants.LOGIN_SUCCESS) {
                        actions.setFieldError('username', 'Invalid Credentials');
                        return;
                    }

                    if (data.type === userConstants.LOGIN_SUCCESS) {
                        Router.push('/');
                        return;
                    }
                });
            }
        }
    });

    return (
        <GuessLayoutHoc>

            <div className="card o-hidden border-0 shadow-lg my-5 mt-5 ">
                <div className="card-body p-0 ">
                    <div className="row">
                        <div className="col-lg-12 col-md-offset-8">
                            <div className="p-5">
                                <div className="text-center mb-3">
                                    <img src="/static/assets/images/logo.svg" alt="logo" width="100" />
                                </div>
                                <form className="user needs-validation" onSubmit={handleSubmit}>
                                    <div className="form-group has-error has-feedback">
                                        <input
                                            type="text"
                                            name="username"
                                            className={`form-control ${errors.username ? 'is-invalid' : null}`}
                                            onChange={handleChange}
                                            placeholder="Username"
                                            values={values.username}
                                        />
                                        {errors.username ? <div className="invalid-feedback pl-2"> {errors.username} </div> : null}
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="password"
                                            name="password"
                                            className={`form-control ${errors.password && 'is-invalid'}`}
                                            onChange={handleChange}
                                            placeholder="Password"
                                            values={values.password}
                                        />

                                        {errors.password ? <div className="invalid-feedback pl-2"> {errors.password} </div> : null}
                                    </div>
                                    <button type="submit" className="btn btn-success btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </GuessLayoutHoc>
    )
}
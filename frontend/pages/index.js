import AdminLayoutHoc from '../components/Layouts/AdminLayoutHoc';
import { userService } from '../services';
import { statAction } from '../actions';

import PlayerBalance from '../components/App/Dashboard/PlayerBalance';
import Server from '../components/App/Dashboard/Server';
import StatReport from '../components/App/Dashboard/StatReport';
import RakeDisplay from '../components/App/Dashboard/RakeDisplay';
import WalletBalance from '../components/App/Dashboard/WalletBalance';

var width20 = {
  width: '20%',
};

export default function Home({ playerBalance, getStatReports, getBalanceWallet }) {
  return (
    <AdminLayoutHoc>
      <div className="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>
      </div>


      <WalletBalance walletBalance={getBalanceWallet} playerBalance={playerBalance} />


      <div className="row">
        <div className="col-lg-4 mb-4">
          <PlayerBalance data={playerBalance} />
        </div>

        <div className="col-lg-8 mb-4">
          <Server />
        </div>
      </div>

      <div className="row">
        <div className="col-lg-12 mb-4">
          <StatReport stat={getStatReports} />
        </div>
      </div>

      <div className="row">
        <div className="col-lg-12 mb-4">
          <RakeDisplay />
        </div>
      </div>



    </AdminLayoutHoc>
  )
}

Home.getInitialProps = async (ctx) => {

  const token = userService.getToken(ctx); //if token does not exist will redirect to login

  if (!token) {
    return {};
  }

  const playerBalance = await statAction.getPlayerBalance(ctx);
  const getStatReports = await statAction.getStatRakeReports(ctx);
  const getBalanceWallet = await statAction.getWalletBalance(ctx);

  return {
    playerBalance,
    getStatReports,
    getBalanceWallet
  };
}

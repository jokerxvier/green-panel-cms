import '../scss/sb-admin-2.scss';
import "react-datepicker/dist/react-datepicker.css";
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';


NProgress.configure({ showSpinner: false, trickleRate: 0.1, trickleSpeed: 300 });

Router.events.on('routeChangeStart', () => {
    NProgress.start()
})

Router.events.on('routeChangeComplete', () => {
    NProgress.done();
})

Router.events.on('routeChangeError', () => {
    NProgress.done();
})


export default function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
}

// MyApp.getInitialProps = async (ctx) => {
//     const token = userService.getToken(ctx);
//     console.log(token);
//     return {};
// }

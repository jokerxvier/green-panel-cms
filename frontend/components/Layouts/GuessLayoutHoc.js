export default function AdminLayoutHoc(props) {

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-xl-6 col-lg-6 col-md-6 align-self-center">
                    {props.children}
                </div>
            </div>
        </div>
    )
}
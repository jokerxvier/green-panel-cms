export default function AdminFooter() {
    return (
        <footer className="sticky-footer bg-white">
            <div className="container my-auto">
                <div className="copyright text-center my-auto">
                <span>Copyright &copy; SWC 2020</span>
                </div>
            </div>
        </footer>
    )
}


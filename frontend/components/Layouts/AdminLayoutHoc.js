import AdminHeader from "./AdminHeader";
import AdminSidebar from "./AdminSidebar";
import AdminContent from "./AdminContent";
import AdminFooter from "./AdminFooter";

function AdminLayoutHoc(props) {
    return (
        <div id="wrapper">
            <AdminSidebar />
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <AdminHeader />
                    <AdminContent>
                        { props.children }
                    </AdminContent>
                    <AdminFooter />
                </div>
            </div>
        </div>
    )
}

export default AdminLayoutHoc;
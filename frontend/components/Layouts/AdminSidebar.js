import Link from 'next/link';
import { withRouter } from 'next/router';

function AdminContent(props) {
    const { pathname } = props.router;
    return (
        <ul className="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">
            <a className="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                <div className="sidebar-brand-icon rotate-n-15">
                    <i className="fas fa-laugh-wink"></i>
                </div>
                <div className="sidebar-brand-text mx-3">Green Panel</div>
            </a>

            <hr className="sidebar-divider my-0" />

            <li className={['nav-item', pathname === '/' ? 'active' : ''].join(' ')}>

                <Link href="/">
                    <a className="nav-link">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </Link>

            </li>


        </ul>
    )
}


export default withRouter(AdminContent);
export default function AdminContent(props) {
    return (
        <div id="content-wrapper" className="d-flex flex-column">
            <div id="content">
                <div className="container-fluid" style={{minHeight: '93vh'}}>
                    {props.children}
                </div>
            </div>
        </div>
    );
}
import React, { useEffect, useState } from 'react';
import { apiConstants } from '../../../constants';
import { serverAction } from '../../../actions';

const Server = React.memo(() => {

    const [server, setServer] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function fetchData() {
            const server = await serverAction.getAll();
            setServer(server);
        }

        fetchData();

    }, [setServer]);



    return (
        <div className="card shadow  overflow-auto" style={{ "height": "356px" }}>
            <div className="card-body">
                {(server.type === apiConstants.GETALL_SUCCESS) &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th className="m-0 font-weight-bold text-primary">Status</th>
                                <th className="m-0 font-weight-bold text-primary">IP</th>
                                <th className="m-0 font-weight-bold text-primary">Info</th>
                                <th className="m-0 font-weight-bold text-primary">Hosting Company</th>
                            </tr>
                        </thead>
                        <tbody>
                            {server.res.map((item, index) => <Row key={index} item={item} />)}
                        </tbody>
                    </table>
                }

                {(server.type === apiConstants.GETALL_FAILURE) && <center>Oops! Something went wrong!</center>}

            </div>
        </div>
    );


});

const Row = (props) => (
    <tr>
        <td>
            <a href="#" className={props.item.status === 'up' ? 'btn btn-success btn-block' : 'btn btn-danger btn-block'}><i className="fa fa-arrow-up"></i> {props.item.status}</a>
        </td>
        <td>{props.item.ip}</td>
        <td>{props.item.host}</td>
        <td>{props.item.hosting}</td>
    </tr >
)

export default Server;
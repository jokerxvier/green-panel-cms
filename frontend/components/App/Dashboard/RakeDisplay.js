import React, { useState, useEffect, useCallback, useContext } from "react";
import * as _ from 'underscore';
import { formatNumber } from '../../../helpers';
import DatePicker from "react-datepicker";

import { statAction } from '../../../actions';

const formatAmount = n => {
    return formatNumber(parseInt(n) / 100);
}

const RakeDisplay = React.memo(() => {

    const [fromDate, setFromDate] = useState(new Date(Date.now() - 6 * 24 * 60 * 60 * 1000));
    const [toDate, setToDate] = useState(new Date(Date.now() + 24 * 60 * 60 * 1000));
    const [items, setItems] = useState([]);

    useEffect(() => {

        statAction.getStatFromTo(fromDate, toDate).then(data => {
            const res = Object.assign({}, data.res);
            setItems(res);
        });

    }, []);

    const handleSubmit = useCallback(e => {
        e.preventDefault();

        statAction.getStatFromTo(fromDate, toDate).then(data => {
            const res = Object.assign({}, data.res);
            setItems(res);
        });


    }, [fromDate, toDate]);


    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">

                    <div className="input-group">
                        <DatePicker className="form-control border-0 small" selected={fromDate} onChange={value => setFromDate(value)} />
                        <DatePicker className="form-control border-0 small" selected={toDate} onChange={value => setToDate(value)} />
                        <div className="input-group-append">
                            <button className="btn btn-primary" type="button" onClick={handleSubmit}>
                                <i className="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
            <div className="card-body">
                <table className="table table-bordered dataTable" id="dataTable">
                    <thead>
                        <tr>
                            <th className="m-0 font-weight-bold text-primary">Date</th>
                            <th className="m-0 font-weight-bold text-primary">Gross Rake</th>
                            <th className="m-0 font-weight-bold text-primary">Tournaments</th>
                            <th className="m-0 font-weight-bold text-primary">Sngs</th>
                            <th className="m-0 font-weight-bold text-primary">Freerolls</th>
                            <th className="m-0 font-weight-bold text-primary">Krill RakeBack</th>
                            <th className="m-0 font-weight-bold text-primary">Affiliates Payouts</th>
                            <th className="m-0 font-weight-bold text-primary">Salaries</th>
                            <th className="m-0 font-weight-bold text-primary">Player Credits</th>
                            <th className="m-0 font-weight-bold text-primary">Net Rake</th>
                        </tr>
                    </thead>
                    <tbody>

                        {!_.isEmpty(items) && _.map(items, (item, index) => <Row key={index} item={item} period={index} />)}
                        {_.isEmpty(items) && <tr><td colSpan="10">No Available Data</td></tr>}
                    </tbody>
                </table>
            </div>
        </div>
    );
});

const Row = (props) => (

    <tr>
        <td className="m-0 font-weight-bold text-primary">{props.period}</td>
        <td>{formatAmount(props.item.grossRake)} Chips</td>
        <td>{formatAmount(props.item.tournamentRegular)} Chips</td>
        <td>{formatAmount(props.item.tournamentSngs)} Chips</td>
        <td>{formatAmount(props.item.tournamentFreeRolls)} Chips</td>
        <td>{formatAmount(props.item.rakeback)} Chips</td>
        <td>{formatAmount(props.item.affiliatePayout)} Chips</td>
        <td>{formatAmount(props.item.salaries)} Chips</td>
        <td>{formatAmount(props.item.playerCredits)} Chips</td>
        <td>
            {
                formatAmount(
                    _.reduce([
                        props.item.grossRake,
                        props.item.tournamentRegular,
                        props.item.tournamentSngs,
                        props.item.tournamentFreeRolls,
                        props.item.rakeback,
                        props.item.affiliatePayout,
                        props.item.salaries,
                        props.item.playerCredits
                    ], function (memo, num) { return memo + num; }, 0)
                )
            }

        </td>
    </tr >
);

export default RakeDisplay;
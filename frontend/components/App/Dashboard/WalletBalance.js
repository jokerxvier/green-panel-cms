import React from "react";
import { apiConstants } from '../../../constants';
import { formatNumber } from '../../../helpers';

const WalletBalance = React.memo(({ walletBalance, playerBalance }) => {

    let nonHotBalance = 0;
    let hotWalletTotal = 0;
    let wallet;

    if (walletBalance.type === apiConstants.GETALL_SUCCESS) {
        wallet = walletBalance.res['BTC'];

        hotWalletTotal = formatNumber(wallet.confirmed + wallet.unconfirmed);

        nonHotBalance = formatNumber((playerBalance.res.total / 100000000) - hotWalletTotal);
    }



    return (
        <div className="row">

            <div className="col-xl-4 col-md-6 mb-4">
                <div className="card border-left-primary shadow h-100 py-2">
                    <div className="card-body">

                        <div className="row no-gutters align-items-center">
                            <div className="col mr-2">
                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">Non-Hot Wallet Liability</div>
                                {(walletBalance.type === apiConstants.GETALL_SUCCESS) &&
                                    <div className="h5 mb-0 font-weight-bold text-gray-800">{nonHotBalance} <span className="text-success">BTC</span></div>
                                }
                                {(walletBalance.type === apiConstants.GETALL_FAILURE) && <center>Oops! Something went wrong!</center>}
                            </div>
                            <div className="col-auto">
                                <i className="fas fa-wallet fa-2x text-primary"></i>
                            </div>

                        </div>



                    </div>
                </div>
            </div>

            <div className="col-xl-4 col-md-6 mb-4">
                <div className="card border-left-danger shadow h-100 py-2">
                    <div className="card-body">
                        <div className="row no-gutters align-items-center">
                            <div className="col mr-2">
                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">Hot Wallet Liability</div>
                                {(walletBalance.type === apiConstants.GETALL_SUCCESS) &&
                                    <div className="row">
                                        <div className="col">
                                            <div className="text-xs text-gray  text-uppercase mb-1">
                                                <span className="font-weight-bold">Spendable</span>
                                                <br /> {formatNumber(wallet.confirmed)} BTC
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="text-xs text-gray  text-uppercase mb-1">
                                                <span className="font-weight-bold">Unspendable</span>
                                                <br /> {formatNumber(wallet.unconfirmed)} BTC
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="text-xs text-gray  text-uppercase mb-1">
                                                <span className="font-weight-bold">Total</span>
                                                <br /> {hotWalletTotal} BTC
                                            </div>
                                        </div>

                                    </div>
                                }
                                {(walletBalance.type === apiConstants.GETALL_FAILURE) && <center>Oops! Something went wrong!</center>}
                            </div>
                            <div className="col-auto">
                                <i className="fas fa-wallet fa-2x text-danger"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );

});

export default WalletBalance;
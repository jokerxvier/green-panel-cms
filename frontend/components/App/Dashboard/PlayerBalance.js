import React from 'react';
import { formatNumber } from '../../../helpers';
import { apiConstants } from '../../../constants';

const PlayerBalance = React.memo(({ data }) => {

    return (
        <div className="card shadow">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Player Chips</h6>
            </div>
            <div className="card-body">
                {(data.type === apiConstants.GETALL_SUCCESS) &&
                    <div>
                        <h4 className="small font-weight-bold mb-2">Accounts <span className="float-right">{formatNumber(data.res.accounts)}</span></h4>
                        <hr />
                        <h4 className="small font-weight-bold mb-2">Tables <span className="float-right">{formatNumber(data.res.tables)}</span></h4>
                        <hr />
                        <h4 className="small font-weight-bold mb-2">Tournaments <span className="float-right">{formatNumber(data.res.tournaments)}</span></h4>
                        <hr />
                        <h4 className="small font-weight-bold mb-2">Tickets <span className="float-right">{formatNumber(data.res.tickets)}</span></h4>
                        <hr />
                        <h4 className="small font-weight-bold mb-2">Pending Cashout <span className="float-right">{formatNumber(data.res.pendingCashouts)}</span></h4>
                        <hr />
                        <h4 className="small font-weight-bold">Total <span className="float-right">{formatNumber(data.res.total)}</span></h4>
                    </div>
                }

                {(data.type === apiConstants.GETALL_FAILURE) && <center>Oops! Something went wrong!</center>}
            </div>
        </div>
    );


});

export default PlayerBalance;
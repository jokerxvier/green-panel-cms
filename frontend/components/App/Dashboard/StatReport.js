import React from 'react';
import * as _ from 'underscore';
import { formatNumber } from '../../../helpers';
import { statPeriodConstants } from '../../../constants';

const formatAmount = n => {
    return formatNumber(parseInt(n) / 100);
}

const StatReport = React.memo(({ stat }) => {



    return (
        <div className="card shadow">
            <div className="card-body">
                <table className="table table-bordered dataTable" id="dataTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th className="m-0 font-weight-bold text-primary">Gross Rake</th>
                            <th className="m-0 font-weight-bold text-primary">Tournaments</th>
                            <th className="m-0 font-weight-bold text-primary">Sngs</th>
                            <th className="m-0 font-weight-bold text-primary">Freerolls</th>
                            <th className="m-0 font-weight-bold text-primary">Krill Rakeback</th>
                            <th className="m-0 font-weight-bold text-primary">Affiliate Payouts</th>
                            <th className="m-0 font-weight-bold text-primary">Salaries</th>
                            <th className="m-0 font-weight-bold text-primary">Player Credits</th>
                            <th className="m-0 font-weight-bold text-primary">Net Rake</th>
                        </tr>
                    </thead>
                    <tbody>

                        {_.map(stat.res, (item, index) => <Row key={index} item={item} period={index} />)}

                    </tbody>
                </table>
            </div>

        </div>
    );
});

const Row = (props) => (
    <tr>
        <td><h4 className="small font-weight-bold mb-2">{statPeriodConstants[props.period]}</h4></td>
        <td className="small">{formatAmount(props.item.grossRake)}</td>
        <td className="small">{formatAmount(props.item.tournamentRegular)}</td>
        <td className="small">{formatAmount(props.item.tournamentSngs)}</td>
        <td className="small">{formatAmount(props.item.tournamentFreeRolls)}</td>
        <td className="small">{formatAmount(props.item.rakeback)}</td>
        <td className="small">{formatAmount(props.item.affiliatePayouts)}</td>
        <td className="small">{formatAmount(props.item.salaries)}</td>
        <td className="small">{formatAmount(props.item.playersCredits)}</td>
        <td className="small">{formatAmount(props.item.netRake)}</td>
    </tr>
);


export default StatReport;
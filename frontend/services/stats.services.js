import { authHeader, handleResponse } from '../helpers';
import queryString from 'query-string';
import moment from 'moment';

export const statService = {
    getPlayerBalance,
    getStatReports,
    getStatFromTo,
    getBalanceWallet
}


function getPlayerBalance(ctx) {

    const requestOptions = {
        method: 'GET',
        headers: authHeader(ctx)
    };

    return fetch(`${process.env.API_URL}/stats/player-balance`, requestOptions).then(res => handleResponse(res, ctx));
}

function getBalanceWallet(ctx) {

    const requestOptions = {
        method: 'GET',
        headers: authHeader(ctx)
    };

    //http://www.json-generator.com/api/json/get/cgDuPqMiaa?indent=2
    return fetch(`http://www.json-generator.com/api/json/get/cgDuPqMiaa?indent=2`, requestOptions).then(res => handleResponse(res, ctx));
}

function getStatReports(ctx) {

    const periods = [
        'rolling-week',
        'rolling-month',
        'rolling-last-week',
        'rolling-last-month',
        'rolling-2-weeks-ago',
        'rolling-2-months-ago'
    ];

    const types = [
        'affiliate-payouts',
        'salaries',
        'player-credits',
        'rakeback',
        'gross-rake',
        'tournament-regular',
        'tournament-sngs',
        'tournament-freerolls'
    ];

    const data = {
        period: periods,
        type: types
    }

    const requestOptions = {
        method: 'GET',
        headers: authHeader(ctx),
    };


    return fetch(`${process.env.API_URL}/stats?${queryString.stringify(data)}`, requestOptions).then(res => handleResponse(res, ctx));
}



function getStatFromTo(from, to, ctx = false) {

    const requestOptions = {
        method: 'GET',
        headers: authHeader(ctx)
    };

    const start = moment(from).format('YYYY-MM-DD');
    const end = moment(to).format('YYYY-MM-DD');

    const types = [
        'affiliate-payouts',
        'salaries',
        'player-credits',
        'rakeback',
        'gross-rake',
        'tournament-regular',
        'tournament-sngs',
        'tournament-freerolls'
    ];

    const data = {
        from: start,
        to: end,
        type: types
    }

    return fetch(`${process.env.API_URL}/stats?${queryString.stringify(data)}`, requestOptions).then(res => handleResponse(res, ctx));
}

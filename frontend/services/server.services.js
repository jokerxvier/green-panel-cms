import { authHeader, handleResponse } from '../helpers';

export const serverService = {
    getAll
}


function getAll(ctx) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(ctx)
    };
    return fetch(`${process.env.API_URL}/servers`, requestOptions).then(res => handleResponse(res, ctx));
}
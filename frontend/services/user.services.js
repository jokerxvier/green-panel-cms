import cookies from 'next-cookies';
import { Cookies } from 'react-cookie';
import Router from 'next/router';

export const userService = {
    login,
    logout,
    setToken,
    getToken
}

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${process.env.API_URL}/auth`, requestOptions)
        .then(handleResponse)
        .then(user => {
            return user;
        });
}

function setToken(token) {
    document.cookie = `swc_token=${token}; path=/`;
}

function getToken(ctx = false) {
    let token;

    if (ctx) {
        token = cookies(ctx).swc_token || '';
    } else {
        token = getCookie('swc_token');
    }


    if (!token) {
        if (!ctx.req) {
            Router.replace('/login');
            return {};
        } else {
            ctx.res?.writeHead(302, {
                Location: `/login`
            });
            ctx.res?.end();
            return;
        }
    }

    return token;
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

function logout() {
    const cookies = new Cookies();
    cookies.remove('swc_token');
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
export const statPeriodConstants = {
    latestWeek: 'Current Week',
    lastWeek: 'Last Week',
    lastTwoWeeks: 'Last Two Weeks Ago',
    latestMonth: 'Current Month',
    lastMonth: 'Last Month',
    lastTwoMonths: 'Last Two Month Ago',
};
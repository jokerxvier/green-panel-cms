require('dotenv').config()
const withSass = require('@zeit/next-sass')
module.exports = withSass();
module.exports = {
    env: {
        API_URL: process.env.API_URL
    }
}

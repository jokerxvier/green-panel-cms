import { userService } from '../services';
import Router from 'next/router';

export function handleResponse(response, ctx = false) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                if (ctx && ctx.req) {
                    ctx.res?.writeHead(302, {
                        Location: `/login`
                    });
                    ctx.res?.end();
                    return;
                }

                if (!ctx.req) {
                    userService.logout();
                    location.reload(true);
                    return {};
                }

            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


import { userService } from '../services';

export function authHeader(ctx = false) {
    const token = userService.getToken(ctx);
    if (token) {
        return { 'Authorization': 'Bearer ' + token };
    } else {
        return {};
    }
}


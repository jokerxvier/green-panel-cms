# Green Panel v2

## Cron Jobs
The Green panel fetches rake & payout analytics for the past day (00:00 - 00:00) every night at 1am UTC. This includes:
 - Gross rake
 - Tournament rake
 - SNG rake
 - Freeroll rake
 - Affiliate payouts
 - Salary payouts
 - Player credits

The Panel also keeps hourly records of player balances. This includes:
 - Chips in accounts
 - Chips at tables
 - Chips in tournaments
 - Chips in tickets
 - Chips in pending cashouts
 - Total chips

## Server Metrics
The backend exposes live metrics endpoint (`/servers`) which returns the current state (`up` / `down`) of all servers used by SwC.

## API Docs
When running the backend in any environment, except `production`, it exposes Swagger API documentation at `/api`.
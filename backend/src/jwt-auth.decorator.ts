import { SetMetadata, applyDecorators, UseGuards, HttpStatus } from '@nestjs/common';
import { UserRole } from './user/dto/create-user.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { ApiBearerAuth, ApiResponse, ApiProperty } from '@nestjs/swagger';

class UnauthorizedResponse {
    @ApiProperty({ example: 401 })
    statusCode: number;
    
    @ApiProperty({ example: "Unauthorized" })
    message: string;
}

class ForbiddenResponse {
    @ApiProperty({ example: 403 })
    statusCode: number;
    
    @ApiProperty({ example: "Forbidden" })
    message: string;
}

export const JwtAuth = (...roles: UserRole[]) => applyDecorators(
    SetMetadata('roles', roles),
    UseGuards(JwtAuthGuard),
    ApiBearerAuth(),
    ApiResponse({
        status: HttpStatus.UNAUTHORIZED,
        description: "Unauthorized",
        type: UnauthorizedResponse
    }),
    ApiResponse({
        status: HttpStatus.FORBIDDEN,
        description: "Forbiden",
        type: ForbiddenResponse
    })
)

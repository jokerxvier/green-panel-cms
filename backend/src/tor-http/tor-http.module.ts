import { Module, HttpModule, DynamicModule, HttpService, Injectable, Provider, HttpModuleOptions, Scope } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as torAxios from 'tor-axios';
import * as _ from 'lodash';

import { AXIOS_CONFIG_PROVIDER } from './tor-http.constants';

let tor = null;

const makeTor = (configService: ConfigService) => {
    if(!tor)
    {
        tor = torAxios.torSetup({
            ip: configService.get<string>('TOR_HOST', '127.0.0.1'),
            port: configService.get<number>('TOR_PORT', 9050),
            controlPort: configService.get<number>('TOR_CONTROL_PORT', 9051),
            controlPassword: configService.get<string>('TOR_CONTROL_PASSWORD', '')
        });
    }
    return tor;
}

export interface ITorHttpModuleAsyncOptions {
    useFactory?: (...args: any[]) => any | Promise<any>,
    inject?: Array<any>,
    useValue?: any,
    useExisting?: any,
    useClass?: new(...args: any[]) => any
}

@Injectable({ scope: Scope.TRANSIENT })
export class TorHttpService {

    constructor(private httpService: HttpService)
    {
        httpService.axiosRef.interceptors.response.use(_.identity, async error => {
            if(typeof error.message === 'string' &&
                error.message.includes("Proxy connection timed out")) await tor.torNewSession();
            throw error;
        });
    }

    get tor() { return tor; }
    get http() { return this.httpService; }
}

@Module({
    imports: [
        HttpModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                httpAgent: makeTor(configService).httpAgent(),
                httpsAgent: makeTor(configService).httpsAgent(),
            }),
            inject: [ConfigService]
        })
    ],
    providers: [TorHttpService],
    exports: [TorHttpService]
})
export class TorHttpModule {

    static forRoot(axiosConfig: HttpModuleOptions = {}): DynamicModule
    {
        return {
            module: TorHttpModule,
            imports: [
                HttpModule.registerAsync({
                    useFactory: async (configService: ConfigService) => ({
                        ...axiosConfig,
                        httpAgent: makeTor(configService).httpAgent(),
                        httpsAgent: makeTor(configService).httpsAgent()
                    }),
                    inject: [ConfigService]
                })
            ],
            providers: [
                TorHttpService
            ],
            exports: [TorHttpService]
        };
    }

    static async forRootAsync(asyncOptions: ITorHttpModuleAsyncOptions = { useValue: {} }): Promise<DynamicModule>
    {
        return {
            module: TorHttpModule,
            imports: [
                HttpModule.registerAsync({
                    useFactory: async (configService: ConfigService, axiosConfig: HttpModuleOptions) => ({
                            ...axiosConfig,
                            httpAgent: makeTor(configService).httpAgent(),
                            httpsAgent: makeTor(configService).httpsAgent()
                        }),
                    inject: [ConfigService, AXIOS_CONFIG_PROVIDER],
                    extraProviders: [
                        {
                            provide: AXIOS_CONFIG_PROVIDER,
                            ...asyncOptions
                        } as Provider
                    ]
                })
            ],
            providers: [
                TorHttpService
            ],
            exports: [TorHttpService]
        };
    }

}
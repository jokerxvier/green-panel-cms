import { ExecutionContext, Injectable, Inject } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { TokenService } from './token/token.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt')
{

  constructor(
    @Inject(Reflector) private reflector: Reflector,
    @Inject(TokenService) private tokenService: TokenService
  ) { super(); }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!await super.canActivate(context)) return false;
    const req = context.switchToHttp().getRequest();

    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (roles && roles.length && !roles.includes(req.user.role)) return false;

    const [, token] = req.headers.authorization.split(" ");
    req.user.token = token;
    return this.tokenService.exists(token);
  }
}

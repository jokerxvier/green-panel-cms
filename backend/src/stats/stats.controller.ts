import { Controller, Inject, Get, Query } from '@nestjs/common';
import { ApiResponse, ApiQuery } from '@nestjs/swagger';
import * as moment from 'moment';
import * as _ from 'lodash';

import { PokerDatabaseService } from 'src/poker-database/poker-database.service';
import { JwtAuth } from 'src/jwt-auth.decorator';
import { DateRangeQueryDto, DatePeriod, DatePeriodToRange } from './dto/date-range-query.dto';
import { HotWalletBalanceQueryDto } from './dto/hot-wallet-balance-query.dto';
import { PlayerBalance } from 'src/poker-database/entities/player-balance.entity';
import { HotWalletBalance } from 'src/hot-wallet/entities/hot-wallet-balance.entity';
import { HotWalletService } from 'src/hot-wallet/hot-wallet.service';
import { StatisticType } from 'src/poker-database/entities/statistic.entity';
import { StatisticHistoryQueryDto } from './dto/statistic-history-query.dto';

type StatHistoryResponse = {
    [period in DatePeriod | 'custom-period']: {
        [type in StatisticType]: [{ amount: number, createdAt: Date }]
    }
};

type HotWalletBalanceResponse = {
    [coin in string]: [Omit<HotWalletBalance, "coin">] | Omit<HotWalletBalance, "coin">
};

@Controller('stats')
export class StatsController {

    constructor(
        @Inject(PokerDatabaseService) private pokerDatabaseService: PokerDatabaseService,
        @Inject(HotWalletService) private hotWalletService: HotWalletService
    ) {}

    @ApiQuery({
        name: "type",
        enum: StatisticType,
        isArray: true,
        required: false
    })
    @ApiQuery({
        name: "period",
        enum: DatePeriod,
        isArray: true,
        required: false
    })
    @ApiResponse({
        schema: {
            example: {
                "rolling-weekly": {
                    "gross-rake": [
                        { amount: 15, createdAt: moment().toISOString() },
                        { amount: 3, createdAt: moment().toISOString() },
                        { amount: 50, createdAt: moment().toISOString() }
                    ]
                },
                "rolling-monthly": {
                    "gross-rake": [
                        { amount: 10, createdAt: moment().toISOString() },
                        { amount: 34, createdAt: moment().toISOString() },
                        { amount: 24, createdAt: moment().toISOString() }
                    ]
                }
            }
        },
        status: 200,
        description: "Statistics",
        isArray: true
    })
    @Get()
    @JwtAuth()
    async statHistory(
        @Query() { from, to, type, period }: StatisticHistoryQueryDto
    ): Promise<StatHistoryResponse>
    {
        const periods = [];

        if(period && period.length)
        {
            for(let p of period)
            {
                periods.push({
                    from: DatePeriodToRange[p]?.from(),
                    to: DatePeriodToRange[p]?.to(),
                    name: p
                });
            }
        }

        let fromDate = from;
        let toDate = to;

        if(fromDate > toDate)
        {
            const c = fromDate;
            fromDate = toDate;
            toDate = c;
        }

        if(fromDate && toDate)
        {
            periods.push({
                from: new Date(fromDate),
                to: new Date(toDate),
                name: "custom-period"
            });
        }


        const result = {} as StatHistoryResponse;

        for(let { from, to, name } of periods)
        {
            const periodResult = await this.pokerDatabaseService.statHistory(from, to, type);
            result[name] = _.chain(periodResult.map(h => h.toObject()))
                            .groupBy('type')
                            .toPairs()
                            .map(([k, v]) => [k, _.map(v, v =>_.omit(v, 'type'))])
                            .fromPairs();
        }
        
        return result;
    }

    @ApiQuery({
        name: "coin",
        isArray: true,
        required: false
    })
    @ApiResponse({
        schema: {
            example: {
                BTC: { confirmed: 13.75643231, unconfirmed: 0.01757578, createdAt: moment().toISOString() },
                BCH: { confirmed: 13.75643231, unconfirmed: 0.01757578, createdAt: moment().toISOString() }
            }
        },
        status: 200,
        description: "Hot wallet balance (when `from` / `to` are provided, the response contains **arrays** groupped by coin)"
    })
    @Get('wallet-balance')
    @JwtAuth()
    async hotWalletBalance(
        @Query() { from, to, coin }: HotWalletBalanceQueryDto
    ): Promise<HotWalletBalanceResponse>
    {
        if(!from || !to)
        {
            const balance = await this.hotWalletService.getBalance(coin);
            if(!Array.isArray(balance))
            {
                const { coin, ...rest } = balance;
                return { [coin]: rest } as HotWalletBalanceResponse;
            }
            return <HotWalletBalanceResponse><any>(
                _.chain(balance.map(h => h.toObject()))
                    .groupBy('coin')
                    .toPairs()
                    .map(([k, v]) => [ k, _.omit(v[0], 'coin') ])
                    .fromPairs()
            );
        }

        let fromDate = from;
        let toDate = to;

        if(fromDate > toDate)
        {
            const c = fromDate;
            fromDate = toDate;
            toDate = c;
        }

        const balanceHistory = await this.hotWalletService.getBalanceHistory(from, to, coin);
        return <HotWalletBalanceResponse><any>(
            _.chain(balanceHistory.map(h => h.toObject()))
                .groupBy('coin')
                .toPairs()
                .map(([k, v]) => [k, _.map(v, v =>_.omit(v, 'coin'))])
                .fromPairs()
        );
    }

    @ApiResponse({
        type: PlayerBalance,
        status: 200,
        description: "Player balance (**returns an array** when `from` / `to` are present)"
    })
    @Get('player-balance')
    @JwtAuth()
    async playerBalance(@Query() { from, to }: DateRangeQueryDto): Promise<PlayerBalance | PlayerBalance[]>
    {
        if(!from || !to) return this.pokerDatabaseService.livePlayerBalance();

        let fromDate = from;
        let toDate = to;

        if(fromDate > toDate)
        {
            const c = fromDate;
            fromDate = toDate;
            toDate = c;
        }

        return this.pokerDatabaseService.playerBalanceHistory(fromDate, toDate);
    }
}
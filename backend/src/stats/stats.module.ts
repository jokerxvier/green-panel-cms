import { Module } from '@nestjs/common';

import { StatsController } from './stats.controller';
import { PokerDatabaseModule } from 'src/poker-database/poker-database.module';
import { HotWalletModule } from 'src/hot-wallet/hot-wallet.module';

@Module({
  imports: [PokerDatabaseModule, HotWalletModule],
  controllers: [StatsController]
})
export class StatsModule {}

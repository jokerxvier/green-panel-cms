import { IsISO8601, IsOptional, IsArray } from "class-validator";

export class HotWalletBalanceQueryDto {
    
    @IsOptional()
    @IsISO8601()
    from?: Date;

    @IsOptional()
    @IsISO8601()
    to?: Date;

    @IsOptional()
    @IsArray()
    coin?: string[];
}
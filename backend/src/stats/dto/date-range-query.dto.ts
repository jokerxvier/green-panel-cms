import { IsISO8601, IsOptional } from "class-validator";
import * as moment from 'moment';

export enum DatePeriod {
    ROLLING_WEEK = 'rolling-week',
    ROLLING_MONTH = 'rolling-month',
    ROLLING_LAST_WEEK = 'rolling-last-week',
    ROLLING_LAST_MONTH = 'rolling-last-month',
    ROLLING_2_WEEKS_AGO = 'rolling-2-weeks-ago',
    ROLLING_2_MONTHS_AGO = 'rolling-2-months-ago'
}

export const DatePeriodToRange = {
    [DatePeriod.ROLLING_WEEK]: {
        from: () => new Date(moment().utc().subtract(1, 'week').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().format("YYYY-MM-DD"))
    },
    [DatePeriod.ROLLING_MONTH]: {
        from: () => new Date(moment().utc().subtract(1, 'month').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().format("YYYY-MM-DD"))
    },
    [DatePeriod.ROLLING_LAST_WEEK]: {
        from: () => new Date(moment().utc().subtract(2, 'week').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().subtract(1, 'week').format("YYYY-MM-DD"))
    },
    [DatePeriod.ROLLING_LAST_MONTH]: {
        from: () => new Date(moment().utc().subtract(2, 'month').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().subtract(1, 'month').format("YYYY-MM-DD"))
    },
    [DatePeriod.ROLLING_2_WEEKS_AGO]: {
        from: () => new Date(moment().utc().subtract(3, 'week').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().subtract(2, 'week').format("YYYY-MM-DD"))
    },
    [DatePeriod.ROLLING_2_MONTHS_AGO]: {
        from: () => new Date(moment().utc().subtract(3, 'month').format("YYYY-MM-DD")),
        to: () => new Date(moment().utc().subtract(2, 'month').format("YYYY-MM-DD"))
    }
}

export class DateRangeQueryDto {
    @IsOptional()
    @IsISO8601()
    from?: Date;

    @IsOptional()
    @IsISO8601()
    to?: Date;
}
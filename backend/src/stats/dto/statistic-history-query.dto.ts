import { IsOptional, IsArray, IsISO8601, ArrayUnique } from "class-validator";
import * as _ from 'lodash';

import { StatisticType } from "src/poker-database/entities/statistic.entity";
import { DatePeriod } from "./date-range-query.dto";

export class StatisticHistoryQueryDto {

    @IsOptional()
    @IsArray()
    @ArrayUnique()
    period?: DatePeriod[];

    @IsOptional()
    @IsArray()
    @ArrayUnique()
    type?: StatisticType[];

    @IsOptional()
    @IsISO8601()
    from?: Date;

    @IsOptional()
    @IsISO8601()
    to?: Date;
}
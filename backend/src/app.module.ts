import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { TokenModule } from './token/token.module';
import { ServersModule } from './servers/servers.module';
import { PokerDatabaseModule } from './poker-database/poker-database.module';
import { LoggerModule } from './logger/logger.module';
import { StatsModule } from './stats/stats.module';
import { HotWalletModule } from './hot-wallet/hot-wallet.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('DB_URL', 'mongodb://127.0.0.1/green-panel'),
        useCreateIndex: true,
        useUnifiedTopology: true
      }),
      inject: [ConfigService]
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('POKER_DB_HOST', '127.0.0.1'),
        port: configService.get<number>('POKER_DB_PORT', 3306),
        username: configService.get<string>('POKER_DB_USERNAME', 'root'),
        password: configService.get<string>('POKER_DB_PASSWORD', ''),
        database: configService.get<string>('POKER_DB_NAME', 'poker_db')
      }),
      inject: [ConfigService]
    }),
    UserModule,
    AuthModule,
    TokenModule,
    ServersModule,
    PokerDatabaseModule,
    LoggerModule,
    StatsModule,
    HotWalletModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

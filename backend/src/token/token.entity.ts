import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import * as mongooseHidden from 'mongoose-hidden';

import { User } from 'src/user/user.entity';

@Schema({ timestamps: true })
export class Token extends mongoose.Document {

    @Prop({ required: true, unique: true })
    token: string;

    @Prop({ required: true })
    expiresAt: Date;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name, required: true, hide: true })
    user: User;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
TokenSchema.plugin(mongooseHidden(), { hidden: { updatedAt: 'hide' } });
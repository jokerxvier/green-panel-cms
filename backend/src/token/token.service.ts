import { Injectable, Inject } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cron } from '@nestjs/schedule';

import { User } from 'src/user/user.entity';
import { Token } from './token.entity';

@Injectable()
export class TokenService {

    constructor(
        @Inject(JwtService) private jwtService: JwtService,
        @Inject(ConfigService) private configService: ConfigService,
        @InjectModel(Token.name) private tokenModel: Model<Token>
    ) {}

    async generate({ id, username }: User): Promise<Token>
    {
        const expiresInMin = this.configService.get<number>('JWT_EXPIRES_IN_MIN');
        const expiresAt = Date.now() / 1000 + expiresInMin*60;

        const jwtToken = await this.jwtService.sign({ sub: id, username, exp: expiresAt });
        const token = new this.tokenModel({
            token: jwtToken,
            expiresAt: new Date(expiresAt*1000),
            user: id
        });
        return token.save();
    }

    async revoke(token: string): Promise<void>
    {
        await this.tokenModel.deleteOne({ token });
    }

    async find(token: string): Promise<Token | null>
    {
        return this.tokenModel.findOne({ token });
    }

    async exists(token: string): Promise<boolean>
    {
        return (await this.tokenModel.countDocuments({ token })) > 0;
    }

    @Cron("0 0 * * * *")
    async removeExpiredTokens(): Promise<void>
    {
        await this.tokenModel.deleteMany({ expiresAt: { $lte: new Date() } });
    }

}

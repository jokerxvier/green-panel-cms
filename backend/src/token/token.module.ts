import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { Token, TokenSchema } from './token.entity';
import { TokenService } from './token.service';

@Global()
@Module({
    imports: [
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get<string>('JWT_SECRET')
            }),
            inject: [ConfigService]
        }),
        MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }])
    ],
    providers: [TokenService],
    exports: [TokenService]
})
export class TokenModule {}

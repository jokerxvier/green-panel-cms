import { Injectable, Inject, HttpService } from '@nestjs/common';
import { Observable, of, throwError, iif } from 'rxjs';
import { pluck, retry, map, switchMap } from 'rxjs/operators';
import { ApiProperty } from '@nestjs/swagger';

export class Server {
    @ApiProperty({ example: "slavedb" })
    host: string;

    @ApiProperty({ example: "shinjiru" })
    hosting: string;

    @ApiProperty({ example: "111.90.141.111" })
    ip: string;

    @ApiProperty({ example: "up", enum: ["up", "down"] })
    status: string;
}

interface IServerEntry {
    __name__: string;
    host: string;
    hosting: string;
    instance: string;
    ip: string;
    job: string;
}

interface IServerList {
    status: string;
    data: IServerEntry[];
    errorType?: string;
    error?: string;
}

@Injectable()
export class ServersService {

    constructor(
        @Inject(HttpService) private httpService: HttpService
    ) { }

    public getServers(): Observable<Server> {
        return this.httpService.get<IServerList>('/').pipe(
            retry(5),
            pluck('data'),
            switchMap(({ status, data, errorType, error }: IServerList) => (
                iif(
                    () => status === 'success',
                    of(...data),
                    throwError(errorType + "\t" + error)
                )
            )),
            map(({ host, hosting, ip, __name__: status }: IServerEntry): Server => ({ host, hosting, ip, status }))
        );
    }
}

import { Controller, Get, Inject } from '@nestjs/common';
import { reduce } from 'rxjs/operators';

import { ServersService, Server } from './servers.service';
import { JwtAuth } from 'src/jwt-auth.decorator';
import { UserRole } from 'src/user/dto/create-user.dto';

@Controller('servers')
export class ServersController {

    constructor(
        @Inject(ServersService) private serversService: ServersService
    ) {}

    @Get()
    @JwtAuth(UserRole.ADMIN)
    async serverList(): Promise<Server[]>
    {
        return this.serversService.getServers().pipe(
            reduce((servers: Server[], server: Server) => [...servers, server], [])
        ).toPromise();
    }

}

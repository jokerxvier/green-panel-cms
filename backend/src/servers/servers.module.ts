import { Module, HttpModule } from '@nestjs/common';
import { ServersController } from './servers.controller';
import { ServersService } from './servers.service';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        baseURL: configService.get<string>('METRICS_URL'),
        auth: {
          username: configService.get<string>('METRICS_AUTH_USERNAME'),
          password: configService.get<string>('METRICS_AUTH_PASSWORD')
        }
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [ServersController],
  providers: [ServersService]
})
export class ServersModule {}

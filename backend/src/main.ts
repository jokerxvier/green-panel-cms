import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';

import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create(AppModule, { cors: true });
	app.use(helmet());
	app.useGlobalPipes(new ValidationPipe({ transform: true }));

	if(process.env.NODE_ENV !== 'production')
	{
		const options = new DocumentBuilder()
							.setTitle("Green Panel API")
							.setDescription("SwC Green panel API")
							.setVersion("1.0")
							.addBearerAuth({
								type: 'http',
								scheme: 'bearer'
							})
							.build();

		const document = SwaggerModule.createDocument(app, options);
		SwaggerModule.setup('api', app, document);
	}

	await app.listen(3000);
}
bootstrap().catch(console.error);

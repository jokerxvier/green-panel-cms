import { Controller, Inject, Post, UseGuards, Req, Delete, HttpStatus } from '@nestjs/common';

import { TokenService } from 'src/token/token.service';
import { Token } from 'src/token/token.entity';
import { User } from 'src/user/user.entity';
import { LocalAuthGuard } from 'src/local-auth.guard';
import { ApiResponse, ApiProperty, ApiBody } from '@nestjs/swagger';
import { JwtAuth } from 'src/jwt-auth.decorator';
import { LoginDto } from './dto/login.dto';

class OkResponse {
    @ApiProperty({ example: "OK" })
    status: string = "OK";
}

class UnauthorizedResponse {
    @ApiProperty({ example: 401 })
    statusCode: number;
    
    @ApiProperty({ example: "Unauthorized" })
    message: string;
}

@Controller('auth')
export class AuthController {

    constructor(
        @Inject(TokenService) private tokenService: TokenService
    ) {}

    @ApiResponse({
        status: HttpStatus.UNAUTHORIZED,
        description: "Unauthorized",
        type: UnauthorizedResponse
    })
    @Post()
    @ApiBody({
        type: LoginDto,
        required: true
    })
    @UseGuards(LocalAuthGuard)
    async login(@Req() req: Request & { user: User }): Promise<Token>
    {
        return this.tokenService.generate(req.user);
    }

    @ApiResponse({
        status: 200,
        type: OkResponse
    })
    @Delete()
    @JwtAuth()
    async logout(@Req() req: Request & { user: User & { token: string } }): Promise<OkResponse>
    {
        const { token } = req.user;
        await this.tokenService.revoke(token);
        return new OkResponse;
    }

}

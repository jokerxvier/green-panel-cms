import { Injectable, Inject } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { UserService } from 'src/user/user.service';
import { User } from 'src/user/user.entity';

@Injectable()
export class AuthService {

    constructor(
        @Inject(UserService) private userService: UserService
    ) {}

    async validateUser(username: string, password: string): Promise<User | null>
    {
        const user = await this.userService.findByUsername(username);
        if(!user || !await bcrypt.compare(password, user.password)) return null;
        return user;
    }

}

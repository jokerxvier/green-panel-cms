import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongooseHidden from 'mongoose-hidden';

@Schema()
export class HotWalletBalance extends Document {

    @Prop({ required: true })
    coin: string;

    @Prop({ required: true })
    confirmed: number;

    @Prop({ required: true })
    unconfirmed: number;

    @Prop({ required: true, default: Date.now })
    createdAt: Date;

}

export const HotWalletBalanceSchema = SchemaFactory.createForClass(HotWalletBalance);
HotWalletBalanceSchema.plugin(mongooseHidden());
HotWalletBalanceSchema.index({ coin: 1, createdAt: 1 });
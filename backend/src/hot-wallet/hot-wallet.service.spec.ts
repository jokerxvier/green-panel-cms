import { Test, TestingModule } from '@nestjs/testing';
import { HotWalletService } from './hot-wallet.service';

describe('HotWalletService', () => {
  let service: HotWalletService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HotWalletService],
    }).compile();

    service = module.get<HotWalletService>(HotWalletService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

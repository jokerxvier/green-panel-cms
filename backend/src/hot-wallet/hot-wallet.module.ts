import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';

import { TorHttpModule } from 'src/tor-http/tor-http.module';
import { HotWalletService } from './hot-wallet.service';
import { HotWalletBalance, HotWalletBalanceSchema } from './entities/hot-wallet-balance.entity';

@Module({
    imports: [
        TorHttpModule.forRootAsync({
            useFactory: async (configService: ConfigService) => ({
                baseURL: configService.get<string>('WALLET_ONION_URL')
            }),
            inject: [ConfigService]
        }),
        MongooseModule.forFeature([{ name: HotWalletBalance.name, schema: HotWalletBalanceSchema }]),
    ],
    providers: [HotWalletService],
    exports: [HotWalletService]
})
export class HotWalletModule {}

import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as _ from 'lodash';

import { TorHttpService } from '../tor-http/tor-http.module';
import { HotWalletBalance } from './entities/hot-wallet-balance.entity';
import { LoggerService } from 'src/logger/logger.service';

@Injectable()
export class HotWalletService {

    constructor(
        @Inject(LoggerService) private loggerService: LoggerService,
        @Inject(TorHttpService) private torHttpService: TorHttpService,
        @InjectModel(HotWalletBalance.name) private hotWalletBalanceModel: Model<HotWalletBalance>
    ) {}

    async getBalance(coin?: string | string[]): Promise<HotWalletBalance | HotWalletBalance[]>
    {
        try
        {
            const { data } = await this.torHttpService.http.get(`/balance/${!coin || Array.isArray(coin) ? "" : coin}`).toPromise();
            
            if(!coin || Array.isArray(coin))
            {
                const coins = _.keys(data).filter(k => !coin || coin.includes(k));
                return coins.map(coin => new this.hotWalletBalanceModel({
                    ...data[coin],
                    coin
                }));
            }

            return new this.hotWalletBalanceModel({
                ...data,
                coin
            });
        }
        catch(e)
        {
            //console.error(e);
            this.loggerService.error(`Failed loading ${coin || "all"} balance${coin ? "s" : ""}!`, e.stack);
            throw e;
        }
    }

    async getBalanceHistory(from: Date, to: Date, coin?: string | string[]): Promise<HotWalletBalance[]>
    {
        return this.hotWalletBalanceModel.find({
            ...(
                coin ? {
                    coin: Array.isArray(coin) ? { $in: coin } : coin
                 } : {}
            ),
            createdAt: { $gte: from, $lt: to }
        });
    }

    /*async getSupportedCoins(): Promise<string[]>
    {
        try
        {
            const { data } = await this.torHttpService.http.get('/coin').toPromise();
            return data;
        }
        catch(e)
        {
            this.loggerService.error("Failed loading supported coins!", e);
            throw e;
        }
    }*/

    @Cron(CronExpression.EVERY_HOUR, { timeZone: "UTC" })
    async recordBalance()
    {
        try
        {
            const hotWalletBalances = await this.getBalance() as HotWalletBalance[];
            await Promise.all(hotWalletBalances.map((b: HotWalletBalance) => b.save()));
        }
        catch(e)
        {
            this.loggerService.error('Failed recording balance!', e);
        }
    }
}

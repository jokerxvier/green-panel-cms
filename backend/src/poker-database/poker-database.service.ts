import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import * as retry from 'async-retry';
import * as moment from 'moment';
import * as _ from 'lodash';

import { LoggerService } from 'src/logger/logger.service';
import { PlayerBalance } from './entities/player-balance.entity';
import { PokerDatabaseRepository, IGrossRake, ITournamentRake, IAffiliatePayouts, ISalaries, TournamentType, IPlayerCredits, IRakeback } from './poker-database.repository';
import { Statistic, StatisticType } from './entities/statistic.entity';

@Injectable()
export class PokerDatabaseService {

    private livePlayerBalanceCache: PlayerBalance;

    constructor(
        @Inject(PokerDatabaseRepository) private pokerDatabaseRepository: PokerDatabaseRepository,
        @Inject(ConfigService) private configService: ConfigService,
        @Inject(LoggerService) private loggerService: LoggerService,
        @InjectModel(PlayerBalance.name) private playerBalanceModel: Model<PlayerBalance>,
        @InjectModel(Statistic.name) private statisticModel: Model<Statistic>
    ) {
        this.loggerService.setContext(PokerDatabaseService.name);
    }

    async livePlayerBalance(): Promise<PlayerBalance | null>
    {
        const maxCacheAge = this.configService.get<number>('ANALYTICS_LIVE_MAX_AGE_MINS', 3);
        if(
            !this.livePlayerBalanceCache ||
            !this.livePlayerBalanceCache.createdAt ||
            Date.now() - this.livePlayerBalanceCache.createdAt.getTime() >= maxCacheAge*60*1000)
        {
            const playerBalance = await this.pokerDatabaseRepository.getPlayerBalance();
            if(!playerBalance) return null;
            this.livePlayerBalanceCache = new this.playerBalanceModel(playerBalance);
        }
        return this.livePlayerBalanceCache;
    }

    async playerBalanceHistory(from: Date, to: Date): Promise<PlayerBalance[]>
    {
        return this.playerBalanceModel.find({ createdAt: { $gte: from, $lt: to } });
    }

    async statHistory(from: Date, to: Date, type?: StatisticType | StatisticType[]): Promise<Statistic[]>
    {
        return this.statisticModel.find({
            ...(
                type ? {
                    type: Array.isArray(type) ? { $in: type } : type
                 } : {}
            ),
            createdAt: { $gte: from, $lt: to }
        });
    }

    /** Cron jobs */

    @Cron(CronExpression.EVERY_HOUR, { timeZone: "UTC" })
    async recordPlayerBalance()
    {
        try
        {
            const playerBalance = await retry(async () => {
                let playerBalance = await this.pokerDatabaseRepository.getPlayerBalance();
                if(!playerBalance) throw new Error("No player balance");
                return playerBalance;
            }, {
                retries: this.configService.get<number>("ANALYTICS_CRON_MAX_RETRIES", 5)
            });

            const playerBalanceModel = new this.playerBalanceModel(playerBalance);
            await playerBalanceModel.save();
            this.loggerService.log("Player balance recorded");
        }
        catch(e)
        {
            this.loggerService.error("Failed to fetch player balance!");
            throw e;
        }
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordGrossRake()
    {
        await this.makeDailyRecord<IGrossRake>(
            this.pokerDatabaseRepository.getGrossRake.bind(this.pokerDatabaseRepository),
            ({ rake, date }) => [ StatisticType.GROSS_RAKE, rake, date ],
            "Gross rake"
        );
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordTournamentRake()
    {
        await this.makeDailyRecord<ITournamentRake>(
            this.pokerDatabaseRepository.getTournamentRake.bind(this.pokerDatabaseRepository),
            ({ rake, tournamentType, date }) => [
                tournamentType === TournamentType.FREEROLLS ? (
                    StatisticType.TOURNAMENT_FREEROLLS
                ) : (
                    tournamentType === TournamentType.SNGS ? (
                        StatisticType.TOURNAMENT_SNGS
                    ) : ( StatisticType.TOURNAMENT_REGULAR )
                ),
                rake,
                date
            ],
            "Tournament rake"
        );
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordAffiliatePayouts()
    {
        await this.makeDailyRecord<IAffiliatePayouts>(
            this.pokerDatabaseRepository.getAffiliatePayouts.bind(this.pokerDatabaseRepository),
            ({ payouts, date }) => [StatisticType.AFFILIATE_PAYOUTS, payouts, date],
            "Affiliate payouts"
        );
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordSalaries()
    {
        await this.makeDailyRecord<ISalaries>(
            this.pokerDatabaseRepository.getSalaries.bind(this.pokerDatabaseRepository),
            ({ salaries, date }) => [StatisticType.SALARIES, salaries, date],
            "Salaries"
        );
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordPlayerCredits()
    {
        await this.makeDailyRecord<IPlayerCredits>(
            this.pokerDatabaseRepository.getPlayerCredits.bind(this.pokerDatabaseRepository),
            ({ playerCredits, date }) => [StatisticType.PLAYER_CREDITS, playerCredits, date],
            "Player credits"
        );
    }

    @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: "UTC" })
    async recordRakeback()
    {
        await this.makeDailyRecord<IRakeback>(
            this.pokerDatabaseRepository.getRakeback.bind(this.pokerDatabaseRepository),
            ({ rakeback, date }) => [StatisticType.RAKEBACK, rakeback, date],
            "Krill rakeback"
        );
    }

    private async makeDailyRecord<T>(
        fetchRecord: (from: Date, to: Date) => Promise<T[]>,
        toRecord: (result: T) => [StatisticType, number, Date],
        what: string = "Daily record"
    ): Promise<void>
    {
        try
        {
            const from = new Date(moment().utc().subtract(1, 'day').format("YYYY-MM-DD"));
            const to = new Date(moment().utc().format("YYYY-MM-DD"));

            const results = await retry(async () => {
                let result = await fetchRecord(from, to);
                if(!result.length) throw new Error(`No results for ${what}`);
                return result;
            }, {
                retries: this.configService.get<number>("ANALYTICS_CRON_MAX_RETRIES", 5)
            });

            for(let result of results)
            {
                const [ type, amount, date ] = toRecord(result);
                const model = new this.statisticModel({ type, amount, createdAt: date });
                await model.save();
            }
            this.loggerService.log(`${what} saved`);
        }
        catch(e)
        {
            this.loggerService.error(`Failed to fetch ${what}`);
            throw e;
        }
    }
}

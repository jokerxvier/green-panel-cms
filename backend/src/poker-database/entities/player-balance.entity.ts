import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongooseHidden from 'mongoose-hidden';

@Schema()
export class PlayerBalance extends Document {

    @Prop({ required: true })
    accounts: number;

    @Prop({ required: true })
    tables: number;

    @Prop({ required: true })
    tournaments: number;

    @Prop({ required: true })
    tickets: number;

    @Prop({ required: true })
    pendingCashouts: number;

    @Prop({ required: true })
    total: number;

    @Prop({ required: true, default: Date.now })
    createdAt: Date;

}

export const PlayerBalanceSchema = SchemaFactory.createForClass(PlayerBalance);
PlayerBalanceSchema.plugin(mongooseHidden());
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongooseHidden from 'mongoose-hidden';
import * as _ from 'lodash';

export enum StatisticType {
    AFFILIATE_PAYOUTS = "affiliate-payouts",
    SALARIES = "salaries",
    PLAYER_CREDITS = "player-credits",
    RAKEBACK = "rakeback",
    GROSS_RAKE = "gross-rake",
    TOURNAMENT_REGULAR = "tournament-regular",
    TOURNAMENT_SNGS = "tournament-sngs",
    TOURNAMENT_FREEROLLS = "tournament-freerolls"
}

@Schema()
export class Statistic extends Document {

    @Prop({ required: true, enum: _.values(StatisticType) })
    type: StatisticType;

    @Prop({ required: true })
    amount: number;

    @Prop({ required: true, default: Date.now })
    createdAt: Date;
}

export const StatisticSchema = SchemaFactory.createForClass(Statistic);
StatisticSchema.plugin(mongooseHidden());
StatisticSchema.index({ type: 1, createdAt: 1 });
export const SalariesQuery = "select SUM(-f_value) as salaries, " +
"DATE(f_stamp) as date " +
"from t_money_transactions " +
"where DATE(f_stamp) >= DATE(?) and DATE(f_stamp) < DATE(?) " +
"and f_type=50 " + // Custom In
"and f_subtype=2001 " + // Bonus
"and f_money_type='R' " + // Only chips
"group by DATE(f_stamp)";
export const GrossRakeQuery = "select SUM(game.f_rake) as rake, DATE(game.f_started_stamp) as date from t_game game " +
"where DATE(game.f_started_stamp) >= DATE(?) and DATE(game.f_started_stamp) < DATE(?) " +
"group by DATE(game.f_started_stamp), DAY(game.f_started_stamp)";
export const AffiliatePayoutsQuery = "select SUM(-f_value) as payouts, DATE_SUB(DATE(f_stamp), INTERVAL 1 DAY) as date " +
"from t_money_transactions " +
"where DATE(f_stamp)>= DATE(?) and DATE(f_stamp) < DATE(?) " +
"and f_type=50 " + // Custom In
"and f_subtype=903 " + // Affiliate Profit
"and f_money_type='R' " + // Only chips
"group by DATE_SUB(DATE(f_stamp), INTERVAL 1 DAY)";
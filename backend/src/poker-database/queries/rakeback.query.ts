export const RakebackQuery = "select SUM(-rbh.f_rakeback) as rakeback, " +
"DATE_SUB(DATE(rbh.f_stamp), INTERVAL 1 DAY) as date " +
"from poker_webadmin.t_player_rakeback_history rbh " +
"where DATE(rbh.f_stamp) >= DATE(?) and DATE(rbh.f_stamp) < DATE(?) " +
"group by DATE_SUB(DATE(rbh.f_stamp), INTERVAL 1 DAY)";
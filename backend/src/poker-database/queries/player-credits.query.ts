export const PlayerCreditsQuery = "select SUM(-f_value) as playerCredits, " +
"DATE(f_stamp) as date " + 
"from t_money_transactions " +
"where DATE(f_stamp) >= DATE(?) and DATE(f_stamp) < DATE(?) " +
"and (f_type=550 or f_type=551) and f_subtype=0 " +
"and f_money_type='R' " + // Only chips
"group by DATE(f_stamp)";
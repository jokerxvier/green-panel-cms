export const PlayerBalanceQuery = "select *, accounts + tables + tournaments + tickets + pendingCashouts as total from " +
"(select (select COALESCE(sum(f_balance), 0) from t_balance where f_player_id != 0 and f_money_type = 'R') accounts, " +
"(select COALESCE(sum(f_last_amount), 0) from t_cash_recovery where f_money_type = 'R') tables, " +
"(select COALESCE(sum(pit.f_buy_in + pit.f_entry_fee), 0) from t_player_in_tournament as pit JOIN " +
"t_tournament as t on pit.f_tournament_id = t.f_id where pit.f_out_level = -1 and t.f_state != 'C') tournaments, " +
"(select COALESCE(sum(f_amount), 0) from t_ticket where f_status = 'A' and f_player_id != 0) tickets, " +
"(select COALESCE(sum(-f_value), 0) from t_money_transactions where f_type = 87 and f_pended = 4) pendingCashouts) as temp";
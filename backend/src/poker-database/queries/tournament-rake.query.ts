export const TournamentRakeQuery = "select sum(profit) as rake, tournamentType, date from " + 
"(select IF(f_type = 69, f_value, -f_value) as profit, date(f_ended_stamp) as date, IF(f_buy_in = 0, 'F', f_tournament_type) as tournamentType " +
"from t_money_transactions AS mt JOIN t_tournament as t ON mt.f_param_tournament_id = t.f_id " +
"where f_type IN (66, 69, 80, 67, 82, 590, 592, 591, 49) and t.f_ended_stamp >= DATE(?) and t.f_ended_stamp < DATE(?) " +
"UNION ALL " +
"SELECT -f_amount as profit, date(f_ended_stamp) as date, IF(f_buy_in = 0, 'F', f_tournament_type) as tournamentType " +
"FROM t_ticket AS ti JOIN t_tournament AS t ON f_obtain_tournament_id = t.f_id " +
"where t.f_ended_stamp >= DATE(?) and t.f_ended_stamp < DATE(?) "+
"UNION ALL " +
"SELECT f_amount as profit, date(f_ended_stamp) as date, IF(f_buy_in = 0, 'F', f_tournament_type) as tournamentType " +
"FROM t_ticket AS ti JOIN t_tournament AS t ON f_spend_tournament_id = t.f_id " +
"where t.f_ended_stamp >= DATE(?) and t.f_ended_stamp < DATE(?) " +
"UNION ALL " +
"SELECT f_amount as profit, date(f_expiration_date) as date, IF(f_buy_in = 0, 'F', f_tournament_type) as tournamentType " +
"FROM t_ticket AS ti JOIN t_tournament AS t ON f_obtain_tournament_id = t.f_id " +
"where f_status = 'E' and f_expiration_date >= DATE(?) and t.f_ended_stamp < DATE(?) and f_obtain_tournament_id != 0 " +
") as temp group by date, tournamentType";
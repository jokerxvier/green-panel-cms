import { Test, TestingModule } from '@nestjs/testing';
import { PokerDatabaseService } from './poker-database.service';

describe('PokerDatabaseService', () => {
  let service: PokerDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PokerDatabaseService],
    }).compile();

    service = module.get<PokerDatabaseService>(PokerDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

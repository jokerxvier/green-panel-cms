import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { PokerDatabaseService } from './poker-database.service';
import { PokerDatabaseRepository } from './poker-database.repository';
import { PlayerBalance, PlayerBalanceSchema } from './entities/player-balance.entity';
import { StatisticSchema, Statistic } from './entities/statistic.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PlayerBalance.name, schema: PlayerBalanceSchema },
      { name: Statistic.name, schema: StatisticSchema }
    ])
  ],
  providers: [PokerDatabaseService, PokerDatabaseRepository],
  exports: [PokerDatabaseService]
})
export class PokerDatabaseModule {}

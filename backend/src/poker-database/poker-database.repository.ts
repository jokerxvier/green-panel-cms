import { Injectable } from "@nestjs/common";
import { EntityManager } from 'typeorm';
import * as _ from 'lodash';

import { GrossRakeQuery } from "./queries/gross-rake.query";
import { PlayerBalanceQuery } from "./queries/player-balance.query";
import { TournamentRakeQuery } from "./queries/tournament-rake.query";
import { AffiliatePayoutsQuery } from "./queries/affiliate-payouts.query";
import { SalariesQuery } from "./queries/salaries.query";
import { PlayerCreditsQuery } from "./queries/player-credits.query";
import { RakebackQuery } from "./queries/rakeback.query";

export interface IPlayerBalance {
    accounts: number;
    tables: number;
    tournaments: number;
    tickets: number;
    pendingCashouts: number;
    total: number;
}

export interface IGrossRake {
    date: Date;
    rake: number;
}

export enum TournamentType {
    REGULAR = "S",
    SNGS = "G",
    FREEROLLS = "F"
}

export interface ITournamentRake {
    rake: number;
    date: Date;
    tournamentType: TournamentType;
}

export interface IAffiliatePayouts {
    date: Date;
    payouts: number;
}

export interface ISalaries {
    date: Date;
    salaries: number;
}

export interface IPlayerCredits {
    date: Date;
    playerCredits: number;
}

export interface IRakeback {
    date: Date;
    rakeback: number;
}

@Injectable()
export class PokerDatabaseRepository {

    constructor(
        private entityManager: EntityManager
    ) {}

    async getPlayerBalance(): Promise<IPlayerBalance | null>
    {
        const [playerBalance] = await this.entityManager.query(PlayerBalanceQuery);
        if(!playerBalance) return null;
        return <IPlayerBalance><unknown>_.fromPairs(_.toPairs(playerBalance).map(([ key, value ]) => [key, Number(value)]));
    }

    async getGrossRake(from: Date, to: Date): Promise<IGrossRake[]>
    {
        const grossRake = await this.entityManager.query(GrossRakeQuery, [from, to]);
        return grossRake.map(({ rake, ...rest }) => ({ ...rest, rake: Number(rake) }));
    }

    async getTournamentRake(from: Date, to: Date): Promise<ITournamentRake[]>
    {
        const tournamentRake = await this.entityManager.query(TournamentRakeQuery, [from, to, from, to, from, to, from, to]);
        return tournamentRake.map(({ profit, ...rest }) => ({ ...rest, profit: Number(profit) }));
    }

    async getAffiliatePayouts(from: Date, to: Date): Promise<IAffiliatePayouts[]>
    {
        const affiliatePayouts = await this.entityManager.query(AffiliatePayoutsQuery, [from, to]);
        return affiliatePayouts.map(({ payouts, ...rest }) => ({ ...rest, payouts: Number(payouts) }));
    }

    async getSalaries(from: Date, to: Date): Promise<ISalaries[]>
    {
        const salaries = await this.entityManager.query(SalariesQuery, [from, to]);
        return salaries.map(({ salaries, ...rest }) => ({ ...rest, salaries: Number(salaries) }));
    }

    async getPlayerCredits(from: Date, to: Date): Promise<IPlayerCredits[]>
    {
        const playerCredits = await this.entityManager.query(PlayerCreditsQuery, [from, to]);
        return playerCredits.map(({ playerCredits, ...rest }) => ({ ...rest, playerCredits: Number(playerCredits) }));
    }

    async getRakeback(from: Date, to: Date): Promise<IRakeback[]>
    {
        const rakeback = await this.entityManager.query(RakebackQuery, [from, to]);
        return rakeback.map(({ rakeback, ...rest }) => ({ ...rest, rakeback: Number(rakeback) }));
    }

}
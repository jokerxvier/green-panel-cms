import { IsNotEmpty, IsOptional, IsString, IsIn, IsByteLength } from 'class-validator';

export enum UserRole {
    ADMIN = 'admin',
    EXTERNAL = 'external'
}

export class CreateUserDto {
    @IsNotEmpty()
    @IsString()
    @IsByteLength(3)
    username: string;

    @IsNotEmpty()
    @IsString()
    @IsByteLength(8)
    password: string;

    @IsOptional()
    @IsString()
    @IsIn(["admin", "external"])
    role?: UserRole = UserRole.EXTERNAL;
}
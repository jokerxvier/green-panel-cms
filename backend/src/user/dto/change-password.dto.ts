import { IsNotEmpty, IsString, IsByteLength } from 'class-validator';

export class ChangePasswordDto {
    @IsNotEmpty()
    @IsString()
    @IsByteLength(8)
    password: string;
}
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiHideProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongooseHidden from 'mongoose-hidden';
import * as bcrypt from 'bcrypt';
import * as _ from 'lodash';

import { UserRole } from './dto/create-user.dto';

@Schema({ timestamps: true })
export class User extends Document {
    
    @Prop({ required: true, unique: true })
    username: string;

    @Prop({ required: true, set: v => bcrypt.hashSync(v, 10), hide: true })
    @ApiHideProperty()
    password: string;

    @Prop({ required: true, type: String, enum: _.values(UserRole), default: UserRole.EXTERNAL })
    role: UserRole = UserRole.EXTERNAL;
}

export const UserSchema = SchemaFactory.createForClass(User);
UserSchema.plugin(mongooseHidden({ hidden: { createdAt: true, updatedAt: true } }));
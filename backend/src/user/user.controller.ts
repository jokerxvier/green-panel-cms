import { Controller, Inject, Post, Body, Get, Req } from '@nestjs/common';

import { UserService } from './user.service';
import { CreateUserDto, UserRole } from './dto/create-user.dto';
import { User } from './user.entity';
import { ChangePasswordDto } from './dto/change-password.dto';
import { JwtAuth } from 'src/jwt-auth.decorator';

@Controller('user')
export class UserController {

    constructor(
        @Inject(UserService) private userService: UserService
    ) {}

    @Post()
    @JwtAuth(UserRole.ADMIN)
    async register(@Body() createUserDto: CreateUserDto): Promise<User>
    {
        return this.userService.create(createUserDto);
    }

    @Get()
    @JwtAuth()
    async me(@Req() req: Request & { user: User }): Promise<User>
    {
        return req.user;
    }

    @Post('/passwd')
    @JwtAuth()
    async changePassword(
        @Body() { password }: ChangePasswordDto,
        @Req() req: Request & { user: User }
    ): Promise<User>
    {
        await this.userService.passwd(req.user, password);
        return req.user;
    }

}

import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserService implements OnApplicationBootstrap {

    constructor(
        @InjectModel(User.name) private userModel: Model<User>
    ) {}

    async onApplicationBootstrap(): Promise<void>
    {
        const userCount = await this.userModel.countDocuments();
        if(userCount < 1)
        {
            const user = new this.userModel({
                username: "admin",
                password: "admin123",
                role: "admin"
            });
            await user.save();
        }
    }

    async create(createUserDto: CreateUserDto): Promise<User>
    {
        const user = new this.userModel(createUserDto);
        return user.save();
    }

    async findByUsername(username: string): Promise<User>
    {
        return this.userModel.findOne({ username });
    }

    async findById(id: string): Promise<User>
    {
        return this.userModel.findById(id);
    }

    async passwd({ id }: User, newPassword: string): Promise<void>
    {
        await this.userModel.updateOne({ _id: id }, { $set: { password: newPassword } });
    }

}
